import java.util.*;
public class sim_interest
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner (System.in);
        char c = ' ';
        do
        {
            System.out.println("Enter the principal amount: ");
            double p = sc.nextDouble();
            System.out.println("Enter the Rate of interest: ");
            double r = sc.nextDouble();
            System.out.println("Enter the Time: ");
            double t = sc.nextDouble();
            double si = (p*r*t)/100 ;
            double a = si + p ;
            System.out.println("The Simple Interest is: " + si);
            System.out.println("The Amount is: "+a);
            System.out.println("Do you want to try again? (y or n): ");
            c = sc.next().charAt(0);
        }
        while (c == 'y');
    }
}

