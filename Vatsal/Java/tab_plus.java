import java.util.*;
public class tab_plus
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner (System.in);
		char x = ' ';
		do
		{
	                System.out.println("Enter a number: ");
		        int num = sc.nextInt();
		        System.out.println("Enter the extent of tables: ");
                        int ext = sc.nextInt();
			for(int i=1 ; i<=ext ; i++)
			{
				System.out.println(num+" * "+i+" = "+(num*i));
			}
			System.out.println("Do you want to try again? (y or n): ");
			x = sc.next().charAt(0);
		}
		while(x == 'y');
	}
}

