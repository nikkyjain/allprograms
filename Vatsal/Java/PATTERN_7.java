import java.util.*;
public class pattern_7
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner (System.in);
        char x=' ';
        do
        {
            System.out.println("Enter the number of rows required in the parallelogram pattern: ");
            int num = sc.nextInt();
            int a=1;
            for(int i=1 ; i<=num ; i++)
            {
                for(int k=1 ; k<=(num-i) ; k++)
                    System.out.print("  ");
                for(int j=1 ; j<=i ; j++)
                {
                    System.out.printf("%02d",a);
                    System.out.print(" ");
                }
                a++;
                System.out.println();
            }
            int n = num-1;
            a=num-1;
            for(int i=n ; i>=1 ; i--)
            {
                System.out.print(" ");
                for(int k=1 ; k<=(n-i) ; k++)
                    System.out.print(" ");
                for(int j=1 ; j<=i ; j++)
                {
                    System.out.printf("%02d",a);
                    System.out.print(" ");
                }
                a--;
                System.out.println();
            }
            System.out.println("Do you want to try again? (y or n): ");
            x = sc.next().charAt(0);  
        }
        while(x=='y');
    }
}
