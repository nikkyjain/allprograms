import java.util.*;
public class pattern_2
{
    public static void main (String args[])
    {
        Scanner sc = new Scanner(System.in);
        char x = ' ';
        do
        {
            System.out.println("Enter the number of terms required in the series: ");
            int a = sc.nextInt();int col=1;
            for (int i=1 ; i<=a ; i++)
            {
                for(int k=1 ; k<=(a-i) ; k++)
                {
                    System.out.print(" ");
                }
                for (int j=1 ; j<=i ; j++)
                {
                    System.out.print(col+" ");
                    col+=2;
                }
                System.out.println();
            }
            System.out.println("Do you want to try again? (y or n): ");
            x = sc.next().charAt(0);
        }
        while(x == 'y');
    }
}

    
