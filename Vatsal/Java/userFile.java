import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class userFile
{
	public static void main(String args[])
	{
		try
		{
		        Scanner sc = new Scanner(System.in);
	         	char x = ' ';
                        FileWriter MyFile = new FileWriter("user_file.txt");
		        MyFile.write("NAME \t AGE \n");
		        do
		        {
		        	System.out.println("Enter name: ");
			        System.out.println("Enter age: ");
				String name = sc.nextLine();
		        	int age = sc.nextInt();
				MyFile.write(name+"\t"+age+"\n");
				System.out.println("Successfully written in the file!");
	                        System.out.println("Do you want to try again? (y or n): ");
		        	x = sc.next().charAt(0);
		        }
		        while(x == 'y');
		        MyFile.close();
		}
		catch(IOException e)
		{
			System.out.println("Something went wrong!");
			e.printStackTrace();
		}

	}
}

