import java.util.*;
public class tables
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner (System.in);
        char a = ' ';
        do
        {
            System.out.println("Enter a number: ");
            long num = sc.nextLong();
            for (int i=1 ; i<=10 ; i++)
            {
                System.out.println(num + " + " + i + " = " + (num*i));
            }
            System.out.println("Do you want to try again? (y or n)");
            a = sc.next().charAt(0);
        }
        while (a == 'y');
    }
}

