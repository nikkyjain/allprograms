import java.util.*;
import java.lang.ArrayIndexOutOfBoundsException;
public class array_list
{
        public static void main (String args[])
        {
                Scanner sc = new Scanner(System.in);
                ArrayList<String> myArray = new ArrayList<String>();
                char x = ' ';
                int rem=0;
                int change=0;
                String changed;
                do
                {
                        System.out.println("Hi i am your personal array!");
                        System.out.println("Enter 'e' to add elements into me! ");
                        System.out.println("Enter 'r' to remove elements from me!");
                        System.out.println("Enter 's' to sort the elements you entered into me!");
                        System.out.println("Enter 'q' to clear all the elements from me!");
                        System.out.println("Enter 'c' to change an element from the listed elements!");
                        char a = sc.next().charAt(0);
                        if(a == 'e')
                        {
                                System.out.println("Your current array: ");
                                System.out.println(myArray);
                                System.out.println();
                                System.out.println("What do you want to add into me ?");
                                String add = sc.next();
                                myArray.add(add);
                                System.out.println("Element "+add+" has been added into me!");
                        }
                        else if(a == 'r')
                        {
                                System.out.println("You current array: ");
                                System.out.println(myArray);
                                try
                                {
                                        System.out.println("What is the number of the element that you want to remove from me ?");
                                        rem = sc.nextInt();
                                        myArray.remove(rem-1);
                                        System.out.println("Element number "+rem+" is removed from me!");
                                }
                                catch(ArrayIndexOutOfBoundsException e)
                                {
                                        System.out.println("Element number "+rem+" does not exist in me!");
                                        e.printStackTrace();
                                }
                        }
                        else if(a == 's')
                        {
                                System.out.println("Your current array: ");
                                System.out.println(myArray);
                                System.out.println();
                                Collections.sort(myArray);
                                System.out.println("All the elements in me are sorted according to the alphabetical order!");
                        }
                        else if(a == 'q')
                        {
                                System.out.println("Your current array: ");
                                System.out.println(myArray);
                                System.out.println();
                                System.out.println("ARE YOU SURE YOU WANT TO CLEAR ALL OF THE ELEMENTS FROM ME? (y or n): ");
                                char clear = sc.next().charAt(0);
                                if(clear == 'y')
                                {
                                        myArray.clear();
                                        System.out.println("All the elements are cleared from me!");
                                }
                                else
                                        continue;
                        }
                        else if(a == 'c')
                        {
                                System.out.println("Your current array: ");
                                System.out.println(myArray);
                                try
                                {
                                        System.out.println("Which number element do you want to change?");
                                        change = sc.nextInt();
                                        System.out.println("What do you want to change it into?");
                                        changed = sc.next();
                                        myArray.set((change-1),changed);
                                        System.out.println("Element number "+change+" has been changed into "+changed+"!");
                                }
                                catch(ArrayIndexOutOfBoundsException e)
                                {
                                        System.out.println("Element number "+change+" does not exist in me!");
                                        e.printStackTrace();
                                }
                        }
                        System.out.println(myArray);
                        System.out.println("Do you want to continue edditing your array? (y or n): ");
                        x = sc.next().charAt(0);
                }
                while(x == 'y');
                System.out.println();
                System.out.println();
                System.out.println("Your final array: ");
                System.out.println(myArray);
        }
}
