import java.util.*;
public class fibonacci
{
	public static void main (String args[])
	{
		Scanner sc = new Scanner (System.in);
		char x = ' ';
		do
		{
		        System.out.println("Enter the number of terms needed in the fibonacci series: ");
		        int num = sc.nextInt();
		        int term = 0;
		        int term1 = 1;
		        System.out.println("First " + num + " terms:");
		        for (int i=1 ; i<=num ; ++i)
		        {
		        	int sum = term + term1;
			        term = term1;
		        	term1 = sum;
	                        System.out.println(sum);
			}
		        System.out.println("Do you want to try again? (y or n): ");
                        x = sc.next().charAt(0);
		}
	        while(x == 'y');
	}
}
