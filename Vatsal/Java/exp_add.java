import java.util.*;
public class exp_add
{
    public static void main (String srgs[])
    {
        Scanner sc = new Scanner (System.in);
        char x = ' ';
        do
        {
            System.out.println("Enter the value to find the sum of the square of all the numbers before the entered number:");
            int n = sc.nextInt();
            double sum = 0;
            for (int i=1 ; i<=n ; i++)
            {
                sum = sum+Math.pow(i,2);
            }
            System.out.println("The answer is: "+sum);
            System.out.println("Do you want to try again?(y or n):");
            x = sc.next().charAt(0);
        }
        while(x == 'y');
    }
}
    
