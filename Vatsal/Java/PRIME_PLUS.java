import java.util.*;
public class prime_plus
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);
        char x=' ';
        do
        {
            System.out.println("Enter a value: ");
            int n = sc.nextInt();
            System.out.println("The prime numbers between 1 and " + n + " are: ");
            System.out.println(2);
            for(int i=2 ; i<=n ; i++)
            {
                for(int j=2 ; j<i; j++)
                {
                    if(i%j==0) 
                        break;
                    else if (j==i-1) 
                        System.out.println(i);
                }
            }
            System.out.println("Do you want to try again? (y or n): ");
            x = sc.next().charAt(0);
        }
        while(x == 'y');
    }
}
