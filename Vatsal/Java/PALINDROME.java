import java.util.*;
public class palindrome
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("This program finds out if a number is a palindrome number or not !");
        System.out.println("A palindrome number is a number which is read same when its digits are reversed.");
        char x = ' ';
        do
        {
            System.out.println("Enter a value; ");
            int val = sc.nextInt();
            int value = val;
            int d ;int sum=0 ;
            int rev = 0;
            while(val>0)
            {
                d = val%10;
                rev = (rev * 10)+d;
                val=val/10;
            }
            if(rev==value)
            System.out.println("The value entered is a palindrome number! ");
            else
            System.out.println("The value entered is not palindrome number! ");
            System.out.println("Do you want to try again? (y or n): ");
            x = sc.next().charAt(0);
        }
        while(x == 'y');
    }
}
