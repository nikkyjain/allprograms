import java.io.*;
class mixOF9th
{
    public static void main(String args[])throws IOException
    {
        BufferedReader buffy=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("MENU");
        System.out.println("1.find the greater number");
        System.out.println("2.find the smaller number");
        System.out.println("3.round off to the nearest whole number");
        System.out.println("4.round off to the integer greater than or equal to the number");
        System.out.println("5.round off to the integer smaller or equal to the number");
        System.out.println("6.find the square root of the number");
        System.out.println("7.find the cube root of the number");
        System.out.println("8.find the power of the number");
        System.out.println("enter the choice");
        String a=buffy.readLine();
        int ch=Integer.parseInt(a);
        System.out.println("enter the first number");
        String b=buffy.readLine();
        double n1=Double.parseDouble(b);
        switch (ch)
        {
        case 1:
        {
            System.out.println("enter the second number");
            String c=buffy.readLine();
            double n=Double.parseDouble(c);
            double max=Math.max(n1,n);
            System.out.println(max);
            break;
        }
        case 2:
        {
            System.out.println("enter the second number");
            String d=buffy.readLine();
            double n2=Double.parseDouble(d);
            double min=Math.min(n1,n2);
            System.out.println(min);
            break;
        }
        case 3:
        {
            double r=Math.round(n1);
            System.out.println(r);
            break;
        }
        case 4:
        {
            double ce=Math.ceil(n1);
            System.out.println(ce);
            break;
        }
        case 5:
        {
            double f=Math.floor(n1);
            System.out.println(f);
            break;
        }
        case 6:
        {
            double sq=Math.sqrt(n1);
            System.out.println(sq);
            break;
        }
        case 7:
        {
            Double cb=Math.cbrt(n1);
            System.out.println(cb);
            break;
        }
        case 8:
        {
            System.out.println("enter the indices");
            String h=buffy.readLine();
            int n3=Integer.parseInt(h);
            double po=Math.pow(n1,n3);
            System.out.println(po);
        }
    }
}
}