import java.io.*;
class BigCal
{
    public static void main(String args[])throws IOException
    {
        BufferedReader buffy=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1.Find the bigger number");
        System.out.println("2.Find the smaller number");
        System.out.println("3.Round the number");
        System.out.println("4.round off to the integer that is greater or equal to itself");
        System.out.println("5.round off to the integer that is smaller or equal to itself");
        System.out.println("6.to find the square root of the number");
        System.out.println("7.to find the cube  root of the number ");
        System.out.println("8.to find the power of the number");
        System.out.println("9.to return the magnitude after ignoring the sign");
        System.out.println("enter the choice");
        String a=buffy.readLine();
        double ch=Integer.parseInt(a);
        System.out.print("enter the first number");
        String b=buffy.readLine();
        double n1=Integer.parseInt(b);
        if (ch==1)
        {
            System.out.println("enter your second number");
            String c= buffy.readLine();
            double n2=Integer.parseInt(c);
            double x=Math.max(n1,n2);
            System.out.println("the greater number is="+x);
        }
        if (ch==2)
        {
            System.out.println("enter your second number");
            String d= buffy.readLine();
            double n3=Integer.parseInt(d);
            double y=Math.min(n1,n3);
            System.out.println("the smaller number is="+y);
        }
        if (ch==3)
        {
            double e=Math.round(n1);
            System.out.println("the number when rounded off is="+e);
        }
        if (ch==4)
        {
            double f=Math.ceil(n1);
            System.out.println("the number when rounded off to the greater or equal integer="+f);
        }
        if (ch==5)
        {
            double g=Math.floor(n1);
            System.out.println("the number when rounded off to the smaller or equal integer="+g);
        }
        if (ch==6)
        {
             double h=Math.sqrt(n1);
            System.out.println("the square root of the number="+h);
        }
        if (ch==7)
        {
             double i=Math.cbrt(n1);
            System.out.println("the cube root of the number="+i);
        }
        if (ch==8)
        {
            System.out.println("enter your index");
            String j= buffy.readLine();
            double n4=Integer.parseInt(j);
            double k=Math.pow(n1,n4);
            System.out.println("the power of the number ="+k);
        }
        if (ch==9)
        {
            double l=Math.abs(n1);
            System.out.println("the number ignoring the sign="+l);
        }
    }
}