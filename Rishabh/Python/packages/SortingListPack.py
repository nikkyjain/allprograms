def insertion_sort(list2):  
        for i in range(1, len(list2)):  
  
            value = list2[i]  
            j = i - 1  
            while j >= 0 and value < list2[j]:  
                list2[j + 1] = list2[j]  
                j -= 1  
            list2[j + 1] = value  
        return(list2)