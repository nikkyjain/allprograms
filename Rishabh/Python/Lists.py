import os
import  sys
import random
from math import*
heroes=["harry","percy","magnus","ronald","eragon"]
villains=["Voldemort","kronos","fenris","malfoy","galbatorix"]
print("heroes: ")
print(heroes)
print()
print("villains: ")
print(villains)
print()
print("printing 3rd hero in the list: ")
print(heroes[2])
print()
print("printing 4th villain in the list: ")
print(villains[3])
print()
everyone=[heroes,villains]
print("list of heroes with villains:")
print(everyone)
print()
heroes.insert(1,"batman")
villains.insert(1,"joker")
print("Another hero added at position 2: ")
print(heroes)
print()
print("Another villain added to position 2:")
print(villains)
print()
print("Sorting the list of heroes and villains in ascending order: ")
heroes.sort()
print(heroes)
print()
villains.sort()
print(villains)




